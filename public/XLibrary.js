let audio = null;

class App
{
    constructor() {}
    static async Refresh()
    {
        await new Promise((resolve, reject) =>
        {
            setTimeout(resolve, 1000 / 60);
        });
    }
    static GetAudioContext()
    {
        return this.GetAudio();
    }
    static GetAudio()
    {
        if (audio === null)
            audio = new AudioContext();
        
        return audio;
    }
}

class Sound
{
    constructor(filePath)
    {
        this.buffer = null;
        
        this.Load(filePath);
    }
    async Load(filePath)
    {
        await new Promise((resolve, reject) =>
        {
            const xhr = new XMLHttpRequest();
            xhr.responseType = "arraybuffer";
            
            const sound = this;
            xhr.onloadend = function()
            {
                if (xhr.status !== 0 && xhr.status !== 200)
                    return;
                
                App.GetAudioContext().decodeAudioData(xhr.response, function(buffer)
                {
                    sound.buffer = buffer;
                    resolve();
                });
            }
            
            xhr.open("GET", filePath, true);
            xhr.send(null);
        });
    }
    Play()
    {
        let source = App.GetAudioContext().createBufferSource();
        source.buffer = this.buffer;
        source.connect(App.GetAudioContext().destination);
        source.start(0);
    }
}
